import React, { memo } from 'react';
import { LoremIpsum } from "lorem-ipsum";
import House from '../images/house-image.png';
import '../css/home.css';

const Home = memo(() => {

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
});

    return (
        <div className="home">
            <a href="/details">Buildings</a>
            <img src={House} alt="house"></img>
            <div className="description">{lorem.generateParagraphs(1)}</div>
        </div>
    );
});

export default Home;
