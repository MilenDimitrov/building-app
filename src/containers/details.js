import { connect } from 'react-redux';
import { getData, addData, updateData, deleteData } from '../actions/data';
import Details from '../components/details';

export default connect((state) => ({
    initialState: state
}), {
    addData,
    getData,
    updateData,
    deleteData
})(Details);
