import React, { memo } from 'react';
import '../css/footer.css';

const Footer = memo(() => {

    return (
        <div className="footer">
            <div className="footer-links">
                <div className="footer-item">
                    <a href="/">Homepage</a>
                </div>
                <div className="footer-item">
                    <a href="/details">Details</a>
                </div> 
            </div>
            <div className="footer-title">The Building App</div>
        </div>
    );
});

export default Footer;
