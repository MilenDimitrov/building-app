import React, { useCallback } from "react";
import { MDBInput } from "mdbreact";
import { ADD, ADD_BUILDING, EDIT, EDIT_BUILDING } from "../constants/constants";
import '../css/input-popup.css';

const InputPopup = (props) => {
    const { addItem, editItem, setShowPopup, currentItem = {} } = props;

    const handleInput = useCallback(() => {
        const id = document.getElementById('input-id').value;
        const name = document.getElementById('input-name').value;
        const area = document.getElementById('input-area').value;
        const location = document.getElementById('input-location').value;
        const image = document.getElementById('input-image').value;

        if (!id || !name || !area) {
            alert("All required fields need to be filled!");
            return;
        }

        if ( name.length < 3 ) {
            alert("Name should be at least 3 characters long!");
            return;
        }

        const newInput = { id: +id, name, area: +area, location, image };
        currentItem ? editItem(newInput) : addItem(newInput)

    }, [addItem, editItem, currentItem]);

    return (
        <div className="form-group">
            <p>{ currentItem ? EDIT_BUILDING : ADD_BUILDING }</p>
            <MDBInput id="input-id" valueDefault={currentItem?.id || ''} label="Id*" size="sm"/>
            <MDBInput id="input-name" valueDefault={currentItem?.name || ''} label="Name*" size="sm"/>
            <MDBInput id="input-area" valueDefault={currentItem?.area || ''} label="Area*" size="sm"/>
            <MDBInput id="input-location" valueDefault={currentItem?.location || ''} label="Location" size="sm"/>
            <MDBInput id="input-image" valueDefault={currentItem?.image || ''} label="Image" size="sm"/>
            <button onClick={handleInput}>{ currentItem ? EDIT : ADD }</button>
            <button onClick={() => setShowPopup(false)}>Close</button>
        </div>
    );
}

export default InputPopup;