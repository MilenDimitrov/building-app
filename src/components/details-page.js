import React, { Fragment, memo, useEffect } from "react";
import Details from "../containers/details";
import '../css/details-header.css';

const DetailsPage = memo((props) => {
const { getData } = props;

useEffect(() => {
    getData();
}, [getData]);

    return (
        <Fragment>
            <Details />
        </Fragment>
    );
});

export default DetailsPage;