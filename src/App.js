import React, { memo } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import DetailsPage from './containers/details-page';
import DetailsHeader from './components/details-header';
import Home from './components/home';
import Footer from './components/footer';
import './App.css';


const App = memo(() => {
    return (
        <div className="App-body">
            <BrowserRouter>
                <DetailsHeader />
                <Switch>
                    <Route path='/' exact component={Home} />
                    <Route path='/details' exact component={DetailsPage} />
                </Switch>
                <Footer/>
            </BrowserRouter>
        </div>
    );
});

export default App;
