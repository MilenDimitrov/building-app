import { createStore as reduxCreateStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers/index';
import sagas from './sagas/index';

// eslint-disable-next-line import/no-anonymous-default-export
export default (initialState) => {
    const sagaMiddleware = createSagaMiddleware();
    const store = reduxCreateStore(reducers, initialState, applyMiddleware(sagaMiddleware));

    sagaMiddleware.run(sagas);

    return store;
};
