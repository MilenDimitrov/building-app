export const BUILDINGS = 'buildings';

export const ADD = 'Add';
export const EDIT = 'Edit';
export const ADD_BUILDING = 'Add a building';
export const EDIT_BUILDING = 'Edit existing building';

export const GET_DATA = 'GET_DATA';
export const ADD_DATA = 'ADD_DATA';
export const UPDATE_DATA = 'UPDATE_DATA';
export const DELETE_DATA = 'DELETE_DATA';