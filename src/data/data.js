import Home from '../images/home.png';
import Home2 from '../images/home 2.png';
import House from '../images/house.png';
import Mansion from '../images/mansion.png';
import Neighborhood from '../images/neighborhood.png';

export const buildingData = [
    {
        id: 1,
        name: 'Windsor',
        area: 200,
        location: 'London',
        image: Home,
    },
    {
        id: 2,
        name: 'Astoria',
        area: 300,
        location: 'Paris',
        image: Home2,
    },
    {
        id: 3,
        name: 'Blue Lagoon',
        area: 150,
        location: 'Amsterdam',
        image: House,
    },
    {
        id: 4,
        name: 'Sofia Estate',
        area: 400,
        location: 'Sofia',
        image: Mansion,
    },
    {
        id: 5,
        name: 'Varna Palace',
        area: 350,
        location: 'Varna',
        image: Neighborhood,
    }
];