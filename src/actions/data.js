import { GET_DATA, ADD_DATA, UPDATE_DATA, DELETE_DATA } from '../constants/constants';

export const getData = () => ({
    type: GET_DATA
});

export const addData = data => ({
    type: ADD_DATA,
    payload: data
});

export const updateData = data => ({
    type: UPDATE_DATA,
    payload: data
});

export const deleteData = id => ({
    type: DELETE_DATA,
    payload: id
});