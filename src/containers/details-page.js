import { connect } from 'react-redux';
import { getData } from '../actions/data';
import DetailsPage from '../components/details-page';

export default connect(null, {
    getData
})(DetailsPage);
