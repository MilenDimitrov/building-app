import { all } from 'redux-saga/effects';
import getData from './data';

// eslint-disable-next-line import/no-anonymous-default-export
export default function* () {
    yield all([
        getData()
    ]);
}
