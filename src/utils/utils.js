export const getStorage = key => {
    try {
        return JSON.parse(localStorage.getItem(key));
    } catch (error) {
        console.log(error);
    }
};

export const setStorage = (key, value) => {
    try {
        localStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
        console.log(error);
    }
};

export const removeStorage = key => {
    localStorage.removeItem(key);
};