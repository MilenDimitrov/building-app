import { takeEvery, put } from 'redux-saga/effects';
import { GET_DATA, BUILDINGS } from '../constants/constants';
import { getStorage } from '../utils/utils';
import { addData } from '../actions/data';
import { buildingData } from '../data/data';

function* onGetData() {
    try {
        const data = getStorage(BUILDINGS) || buildingData;
        yield put(addData(data));
    } catch (error) {
        console.log(error)
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function* () {
    yield takeEvery(GET_DATA, onGetData);
}