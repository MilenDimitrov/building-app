import React, { Fragment, memo, useCallback, useEffect, useState } from 'react';
import { BUILDINGS } from '../constants/constants';
import { setStorage, removeStorage } from '../utils/utils';
import { AiFillDelete } from "react-icons/ai";
import { RiPencilFill } from "react-icons/ri";
import { BsPlus } from "react-icons/bs";
import Home from '../images/home.png';
import InputPopup from './input-popup';
import '../css/details.css';

const Details = memo((props) => {
const { addData, updateData, deleteData, initialState } = props;
const { data } = initialState;
const [state, setState] = useState([]);
const [showPopup, setShowPopup] = useState(false);
const [currentItem, setCurrentItem] = useState(null);

useEffect (() => {
    setState(data);
}, [data] );

const openAddItemPopup = useCallback(() => {
    setCurrentItem(null);
    setShowPopup(true);
}, []);

const openEditItemPopup = useCallback((newItem) => {
    setCurrentItem(newItem)
    setShowPopup(true)
}, []);

const addItem = useCallback((newItem) => {
    const isIdTaken = state.map((item) => item.id).includes(newItem.id)

    if (isIdTaken) {
        alert(`Id's can not duplicate!`)
        return;
    }

    const newState = [...state, newItem];
    addData([newItem])
    setStorage(BUILDINGS, newState);
    setShowPopup(false)
}, [state, addData]);

const editItem = useCallback((newItem) => {
    const newState = state.map((item) => {
        if (item.id === newItem.id) {
            item = {...item, ...newItem};
        }
        return item;
    });

    updateData(newItem)
    setStorage(BUILDINGS, newState);
    setShowPopup(false)
}, [state, updateData]);

const deleteItem = useCallback((id) => {
    const newState = state.filter((item) => item.id !== id);
    deleteData(id);
    setStorage(BUILDINGS, newState);
    if (!newState.length) {
        removeStorage(BUILDINGS);
    }
}, [state, deleteData]);

const renderTableHeader = useCallback(() => {
    return (
        <Fragment>
            <table className="table-header">
                <tbody>
                    <tr>
                        {/* <th><i className="add-item" key="add" onClick={openAddItemPopup}>{<BsPlus/>}</i></th> */}
                        <th className="id">Id<i className="add-item" key="add" onClick={openAddItemPopup}>{<BsPlus/>}</i></th>
                        <th className="name">Name</th>
                        <th className="area">Area</th>
                        <th className="location">Location</th>
                        <th className="image">Image</th>
                        <th className="action">Action</th>
                    </tr>
                </tbody>
            </table>
        </Fragment>
    )
}, [openAddItemPopup]);

const renderTableContent = useCallback((item) => {
    const { id, name, area, location, image } = item;

    return (
        <table key={id} className="table-body">
            <tbody>
                <tr>
                    <th className="id">{ id }</th>
                    <th className="name">{ name }</th>
                    <th className="area">{ area }</th>
                    <th className="location">{ location }</th>
                    <th className="image"><img src={image || Home} alt="house"></img></th>
                    <th className="action">
                        <i key="edit" onClick={() => openEditItemPopup(item)}>{<RiPencilFill/>}</i>
                        <i key="delete" onClick={() => deleteItem(id)}>{<AiFillDelete/>}</i>
                    </th>
                </tr>
            </tbody>
        </table>
    )
}, [deleteItem, openEditItemPopup]);

    return (
        <div className="details">
            {/* <DetailsHeader /> */}
            {renderTableHeader()}
            {showPopup ? 
                <InputPopup 
                    addItem={addItem} 
                    editItem={editItem}
                    currentItem={currentItem}
                    setShowPopup={setShowPopup} 
                    /> : 
                null}
            {state !== [] ? state.map((house) => renderTableContent(house)) : null}
        </div>
    );
});

export default Details;
