import { produce } from 'immer';
import { UPDATE_DATA, ADD_DATA, DELETE_DATA } from '../constants/constants';

const initialState = [];

export default produce((draftState, { type, payload }) => {
    switch (type) {
        case ADD_DATA: {
            return [...draftState, ...payload];
        }
        case UPDATE_DATA: { 
            const newState = draftState.map((item) => {
                if (item.id === payload.id) {
                    item = {...item, ...payload};
                }
                return item;
            });
            return [...newState];
        }
        case DELETE_DATA: {
            const newState = draftState.filter((item) => item.id !== payload);
            return [...newState];
        }
        default: {
            return
        }
    }
}, initialState);

