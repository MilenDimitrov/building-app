import React, { memo } from "react";
import User from '../images/user-icon.png';
import '../css/details-header.css';

const DetailsHeader = memo(() => {

  return (
    <div className="details-header">
        <p>Welcome to The Building App</p>
        <img className="user-icon" src={User} alt="house"></img>
    </div>
    );
});

export default DetailsHeader;