import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createStore from './createStore';
import '@fortawesome/fontawesome-free/css/all.min.css'; 
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import './index.css';
import App from './App';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={createStore()}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
